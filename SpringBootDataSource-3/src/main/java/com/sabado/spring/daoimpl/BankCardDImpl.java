package com.sabado.spring.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.AccountHolderDao;
import com.sabado.spring.dao.BankCardDao;
import com.sabado.spring.entityResponse.AccountHolderEntityResponse;
import com.sabado.spring.entityResponse.Account_Id_Entity_Response;
import com.sabado.spring.entityResponse.BankBranchEntityResponse;
import com.sabado.spring.entityResponse.BankEntityResponse;
import com.sabado.spring.entityResponse.Bank_Id_Entity_Response;
import com.sabado.spring.entityResponse.Branch_Id_Entity_Response;
import com.sabado.spring.entityResponse.CardAccountEntityResponse;
import com.sabado.spring.entityResponse.Card_Id_Entity_Response;
@Repository
public class BankCardDImpl extends Jdbc implements BankCardDao {

	@Override
	public List<Bank_Id_Entity_Response> getBankId() {
		String SQL	= "SELECT bank_id,bank_name from bank" ;

		List<Bank_Id_Entity_Response> bankentity = null;
		try {
			bankentity = jdbcTemplate.query(SQL, new RowMapper<Bank_Id_Entity_Response>() {
				
				
				public Bank_Id_Entity_Response mapRow(ResultSet rs,int i) throws SQLException{
					
					Bank_Id_Entity_Response bankentities = new Bank_Id_Entity_Response();
					
					
				bankentities.setBank_id(rs.getInt("bank_id"));
				bankentities.setBank_name(rs.getString("bank_name"));
										
                 List<Branch_Id_Entity_Response> accountholders = getBranchId(rs.getInt("bank_id"));
					
					bankentities.setBranch(accountholders);
					return bankentities;
					
					
					
				}
			});
			
			
			
			
		}catch(Exception e) {
			System.out.println(e);
			throw e;
			
		}
		return bankentity;
		
		
	}

	

	@Override
	public List<Branch_Id_Entity_Response> getBranchId(int bank_id) {
		String SQL	= "SELECT br.branch_id,br.branch_name,br.ifsc,count(account_id) as account_holders_in_branch from branch as br\r\n" + 
				" inner join account as a on a.branch_id = br.branch_id where br.branch_id = ? ";
		List<Branch_Id_Entity_Response> branchid = null;
		try {
			branchid = jdbcTemplate.query(SQL, new Object[] {bank_id},new RowMapper<Branch_Id_Entity_Response>() {
				
				
				public Branch_Id_Entity_Response mapRow(ResultSet rs,int i) throws SQLException{
					
					Branch_Id_Entity_Response branch = new Branch_Id_Entity_Response();
					
					branch.setBranch_id(rs.getInt("branch_id"));
					branch.setBranch_name(rs.getString("branch_name"));
					branch.setIfsc(rs.getString("ifsc"));
					branch.setAccount_holders_in_branch(rs.getInt("account_holders_in_branch"));
					
				
	               List<Account_Id_Entity_Response> accountid = getAccountId(rs.getInt("branch_id"));
	               
					branch.setAccount(accountid);
					return branch;
		
				}
			});
			
	
		}catch(Exception e) {
			System.out.println(e);
			throw e;
			
		}
		return branchid;
			
		
		       
	}

	@Override
	public List<Account_Id_Entity_Response> getAccountId(int branch_id) {
		 String	SQL	= "select a.account_id,ah.account_holder_name,at.account_type_name,s.status_name from\r\n" + 
			 		"account as a inner join account_holder as ah on a.account_holder_id = ah.account_holder_id inner join \r\n" + 
			 		"account_types as at on a.account_type_id = at.account_type_id inner join\r\n" + 
			 		" status_type s on ah.status_type_id\r\n" + 
			 		"= s.status_type_id where a.branch_id = ?";
			 

				List<Account_Id_Entity_Response> accountid = null;
				try {
					accountid = jdbcTemplate.query(SQL,new Object[] {branch_id}, new RowMapper<Account_Id_Entity_Response>() {
						
						
						public Account_Id_Entity_Response mapRow(ResultSet rs,int i) throws SQLException{
							
							Account_Id_Entity_Response account = new Account_Id_Entity_Response();
						
							account.setAccount_id(rs.getInt("account_id"));
							
							account.setAccount_holder_name(rs.getString("account_holder_name"));
							account.setAccount_type_name(rs.getString("account_type_name"));
							
							account.setStatus_name(rs.getString("status_name"));
							
							
							
					
											
		                 List<Card_Id_Entity_Response> accountentity = getCardId(rs.getInt("account_id"));
							
							account.setCard(accountentity);
							return account;
							
							
							
						}
					});
					
					
					
					
				}catch(Exception e) {
					System.out.println(e);
					throw e;
					
				}
				return accountid;
				
				
	}

	@Override
	public List<Card_Id_Entity_Response> getCardId(int account_id) {
		 String	SQL	= "select ca.account_id,ct.card_type from card_account_card as ca inner join card_types as ct\r\n" + 
			 		"on ca.card_type_id = ct.card_type_id where ca.account_id = ?";
				    List<Card_Id_Entity_Response> type = jdbcTemplate.query(SQL,new Object[] {account_id}, new BeanPropertyRowMapper<Card_Id_Entity_Response>(Card_Id_Entity_Response.class));
					return type;
					
	}

}
