package com.sabado.spring.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.BankTransactionDao;
import com.sabado.spring.entity.BankTransaction;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.entityResponse.BranchNameEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;

@Repository
public class BankTransactionImpl extends Jdbc implements BankTransactionDao {
	

	@Override
	public List<AccountIdNameResponse> getAccountAmount() {
		
	String SQL	= "SELECT ac.account_id,ah.account_holder_name from account as ac inner join account_holder as ah on ac.account_holder_id=ah.account_holder_id" ;

	List<AccountIdNameResponse> accountname = null;
	try {
		accountname = jdbcTemplate.query(SQL, new RowMapper<AccountIdNameResponse>() {
			
			
			public AccountIdNameResponse mapRow(ResultSet rs,int i) throws SQLException{
				
				AccountIdNameResponse accountnames = new AccountIdNameResponse();
				accountnames.setAccount_id(rs.getInt("account_id"));
				accountnames.setAccount_holder_name(rs.getString("account_holder_name"));
				
				List<TransactionAmountEntityResponse> amount = getTransactionAmount(rs.getInt("account_id"));
				
				accountnames.setTransaction(amount);
				return accountnames;
				
				
				
			   
			}
		});
		
		
		
		
	}catch(Exception e) {
		System.out.println(e);
		throw e;
		
	}
	return accountname;
	       
	    
}

	@Override
	public List<TransactionAmountEntityResponse> getTransactionAmount(int account_id) {
    String	SQL	= "SELECT t.transaction_type,t.transaction_amount,t.transaction_date_time from transaction as t inner join account as ac on t.account_id = ac.account_id  where ac.account_id = ?";
    List<TransactionAmountEntityResponse> amount = jdbcTemplate.query(SQL,new Object[] {account_id}, new BeanPropertyRowMapper<TransactionAmountEntityResponse>(TransactionAmountEntityResponse.class));
	return amount;

	}   
}













