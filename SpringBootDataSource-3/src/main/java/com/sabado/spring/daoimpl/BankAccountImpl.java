package com.sabado.spring.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.BankAccDao;
import com.sabado.spring.entity.BankAccount;
import com.sabado.spring.entityResponse.AccountHolderEntityResponse;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountTypesEntityResponse;
import com.sabado.spring.entityResponse.BankBranchEntityResponse;
import com.sabado.spring.entityResponse.BankEntityResponse;
import com.sabado.spring.entityResponse.CardAccountEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;
import com.sabado.spring.entityResponse.TypesEntityResponse;

@Repository
public class BankAccountImpl extends Jdbc implements BankAccDao {
	
	
	@Override
	public List<BankEntityResponse> getBankEntity() {
		String SQL	= "SELECT bank_id,bank_name from bank" ;

		List<BankEntityResponse> bankentity = null;
		try {
			bankentity = jdbcTemplate.query(SQL, new RowMapper<BankEntityResponse>() {
				
				
				public BankEntityResponse mapRow(ResultSet rs,int i) throws SQLException{
					
					BankEntityResponse bankentities = new BankEntityResponse();
					
					
					bankentities.setBank_id(rs.getInt("bank_id"));
					bankentities.setBank_name(rs.getString("bank_name"));
					
										
                 List<AccountHolderEntityResponse> accountholders = getBankAccount(rs.getInt("bank_id"));
					
					bankentities.setBranch_details(accountholders);
					return bankentities;
					
					
					
				}
			});
			
			
			
			
		}catch(Exception e) {
			System.out.println(e);
			throw e;
			
		}
		
		return bankentity;
	
		       
	}


	@Override
	public List<AccountHolderEntityResponse> getBankAccount(int bank_id) {
		String SQL	= "SELECT br.branch_id,br.branch_name,br.ifsc,count(account_id) as account_holders_in_branch from branch as br\r\n" + 
				" inner join account as a on a.branch_id = br.branch_id where br.branch_id = ? ";
		List<AccountHolderEntityResponse> bankentity = null;
		try {
			bankentity = jdbcTemplate.query(SQL, new Object[] {bank_id},new RowMapper<AccountHolderEntityResponse>() {
				
				
				public AccountHolderEntityResponse mapRow(ResultSet rs,int i) throws SQLException{
					
					AccountHolderEntityResponse bankentities = new AccountHolderEntityResponse();
					
					bankentities.setBranch_id(rs.getInt("branch_id"));
					bankentities.setBranch_name(rs.getString("branch_name"));
					bankentities.setIfsc(rs.getString("ifsc"));
					bankentities.setAccount_holders_in_branch(rs.getInt("account_holders_in_branch"));
	               List<BankBranchEntityResponse> accountholders = getBankId(rs.getInt("branch_id"));
					bankentities.setAccounts(accountholders);
					return bankentities;
		
				}
			});
			
	
		}catch(Exception e) {
			System.out.println(e);
			throw e;
			
		}
			
		return bankentity;
		       
	}

	@Override
	public List<BankBranchEntityResponse> getBankId(int branch_id) {
		 String	SQL	= "select a.account_id,ah.account_holder_name,at.account_type_name,s.status_name from\r\n" + 
		 		"account as a inner join account_holder as ah on a.account_holder_id = ah.account_holder_id inner join \r\n" + 
		 		"account_types as at on a.account_type_id = at.account_type_id inner join\r\n" + 
		 		" status_type s on ah.status_type_id\r\n" + 
		 		"= s.status_type_id where a.branch_id = ?";
		 

			List<BankBranchEntityResponse> bankbranch = null;
			try {
				bankbranch = jdbcTemplate.query(SQL,new Object[] {branch_id}, new RowMapper<BankBranchEntityResponse>() {
					
					
					public BankBranchEntityResponse mapRow(ResultSet rs,int i) throws SQLException{
						
						BankBranchEntityResponse bankbranches = new BankBranchEntityResponse();
					
						bankbranches.setAccount_holder_name(rs.getString("account_holder_name"));
						bankbranches.setAccount_id(rs.getInt("account_id"));
						bankbranches.setAccount_type_name(rs.getString("account_type_name"));
						bankbranches.setStatus_name(rs.getString("status_name"));
						
				
										
	                 List<CardAccountEntityResponse> accountentity = getCardName(rs.getInt("account_id"));
						
						bankbranches.setCardaccount(accountentity);
						return bankbranches;
						
						
						
					}
				});
				
				
				
				
			}catch(Exception e) {
				System.out.println(e);
				throw e;
				
			}
			
			return bankbranch;
			
				    
	}

	@Override
	public List<CardAccountEntityResponse> getCardName(int account_id) {
		 String	SQL	= "select ca.account_id,ct.card_type from card_account_card as ca inner join card_types as ct\r\n" + 
		 		"on ca.card_type_id = ct.card_type_id where ca.account_id = ?";
			    List<CardAccountEntityResponse> type = jdbcTemplate.query(SQL,new Object[] {account_id}, new BeanPropertyRowMapper<CardAccountEntityResponse>(CardAccountEntityResponse.class));
				return type;
				
	}

	

	



	}
	    
	    
	    
	    
	    
	    
	   

