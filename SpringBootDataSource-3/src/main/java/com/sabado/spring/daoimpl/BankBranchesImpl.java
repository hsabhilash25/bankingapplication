package com.sabado.spring.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.BankBranchesDao;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.entityResponse.BranchNameEntityResponse;

@Repository
@CrossOrigin()
public class BankBranchesImpl extends Jdbc implements BankBranchesDao{

	@Override
	public List<BankNameEntityResponse> getBankBranch() {
		String SQL = "SELECT bank_id,bank_name from bankingapp.bank";
		List<BankNameEntityResponse> bankname = null;
		
		try {
			bankname = jdbcTemplate.query(SQL, new RowMapper<BankNameEntityResponse>() {
				
				
				public BankNameEntityResponse mapRow(ResultSet rs,int i) throws SQLException{
					
					BankNameEntityResponse banknames = new BankNameEntityResponse();
					banknames.setBank_id(rs.getInt("bank_id"));
					banknames.setBank_name(rs.getString("bank_name"));
					List<BranchNameEntityResponse> branch = getBranchNames(rs.getInt("bank_id"));
					banknames.setBranch(branch);
					return banknames;
					
				   
				}
			});
			
			
		}catch(Exception e) {
			System.out.println(e);
			throw e;
		}
		return bankname;
	}

	@Override
	
	public List<BranchNameEntityResponse> getBranchNames(int bank_id) {
 String	SQL	="select b.branch_name from branch as b where b.bank_id = ?";
	List<BranchNameEntityResponse> branchname = jdbcTemplate.query(SQL,new Object[] {bank_id}, new BeanPropertyRowMapper<BranchNameEntityResponse>(BranchNameEntityResponse.class));
	return branchname;
	
	
	
	}
	
	

}
	
		
		
		
	
    
    
    
   
    


