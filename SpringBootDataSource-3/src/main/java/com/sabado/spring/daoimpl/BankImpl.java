package com.sabado.spring.daoimpl;

import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.BankDao;
import com.sabado.spring.entity.Bank;

@Repository
public class BankImpl extends Jdbc implements  BankDao {


	@Override
	public List<Bank> getBanks() {
	
			 List<Bank> banks = null ;
			  
			  try {
				   banks = jdbcTemplate .query("SELECT * FROM bank",new BeanPropertyRowMapper<Bank>(Bank.class));   
				  } catch (DataAccessException e) {
				   e.printStackTrace();
				  }
				  return banks;
				 }
	



	@Override
	public Bank getBank(int bank_id) {
		 Bank bank = null;
		  try {
		   bank = jdbcTemplate.queryForObject("SELECT * FROM bank WHERE bank_id = ?",
		     new Object[] { bank_id }, new BeanPropertyRowMapper<Bank>(Bank.class));
		  } catch (DataAccessException e) {
		   e.printStackTrace();
		  }
		  return bank;
	}






	@Override
	public int deleteBank(int bank_id) {
		int count = jdbcTemplate.update("DELETE from bank WHERE bank_id = ?", new Object[] { bank_id });
		  return count;
	}






	@Override
	public int updateBank(Bank bank) {
		int count = jdbcTemplate.update(
			    "UPDATE bank set bank_name = ? , location_id = ?,address = ?,ifsc = ?  where bank_id = ?", new Object[] {
			      bank.getBank_name(),bank.getLocation_id(), bank.getAddress(),bank.getIfsc(),  bank.getBank_id() });
			  return count;
	}






	@Override
	public int createBank(Bank bank) {
		int count = jdbcTemplate.update(
			    "INSERT INTO bank(bank_id,bank_name, location_id, address,ifsc,bank_type_id)VALUES(?,?,?,?,?,?)", new Object[] {
			      bank.getBank_id(), bank.getBank_name(), bank.getLocation_id(), bank.getAddress(),bank.getIfsc(),bank.getBank_type_id() });
			  return count;
	}

	
    
    
	
}
