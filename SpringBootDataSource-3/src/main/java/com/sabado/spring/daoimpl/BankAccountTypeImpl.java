package com.sabado.spring.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.BankAccountTypeDao;
import com.sabado.spring.entity.BankAccountType;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountTypesEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;
import com.sabado.spring.entityResponse.TypesEntityResponse;

@Repository
public class BankAccountTypeImpl extends Jdbc implements BankAccountTypeDao {
	
	




	@Override
	public List<AccountTypesEntityResponse> getAccountTypes() {
		String SQL	= "SELECT a.account_id,ah.account_holder_id,ah.account_holder_name from \r\n" + 
				"account as a inner join account_holder as ah on \r\n" + 
				"a.account_holder_id = ah.account_holder_id" ;

		List<AccountTypesEntityResponse> accounttype = null;
		try {
			accounttype = jdbcTemplate.query(SQL, new RowMapper<AccountTypesEntityResponse>() {
				
				
				public AccountTypesEntityResponse mapRow(ResultSet rs,int i) throws SQLException{
					
					AccountTypesEntityResponse accounttypes = new AccountTypesEntityResponse();
					accounttypes.setAccount_holder_id(rs.getInt("account_holder_id"));
					
					accounttypes.setAccount_id(rs.getInt("account_id"));
					accounttypes.setAccount_holder_name(rs.getString("account_holder_name"));
					
					
					
					List<TypesEntityResponse> types = getTypesEntity(rs.getInt("account_holder_id"));
					
					accounttypes.setTypes(types);
					return accounttypes;
					
					
				   
				}
			});
			
			
			
			
		}catch(Exception e) {
			System.out.println(e);
			throw e;
			
		}
		;
		return accounttype;
		       
		    
	}



	@Override
	public List<TypesEntityResponse> getTypesEntity(int account_holder_id) {
		 String	SQL	= "select at.account_type_name from account_types as at inner join\r\n" + 
		 		"account as a on a.account_type_id = at.account_type_id where a.account_holder_id = ?";
		    List<TypesEntityResponse> type = jdbcTemplate.query(SQL,new Object[] {account_holder_id}, new BeanPropertyRowMapper<TypesEntityResponse>(TypesEntityResponse.class));
			return type;
			
	}
		
	}




