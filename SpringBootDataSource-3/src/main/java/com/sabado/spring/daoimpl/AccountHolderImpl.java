package com.sabado.spring.daoimpl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.AccountHolderDao;
import com.sabado.spring.entity.AccountHolder;

@Repository
public class AccountHolderImpl extends Jdbc implements AccountHolderDao {

	
	String SQL1 = "SELECT * FROM account_holder";
	@Override
	public List<AccountHolder> getAccounts() { // getAccountsDetails method which is overrided by accountholderDao interface
		 List<AccountHolder> holders = null ;
		  
		  try {
			   holders = jdbcTemplate.query(SQL1,new BeanPropertyRowMapper<AccountHolder>(AccountHolder.class));  //to map the row of account_holders using bean property 
			  } catch (DataAccessException e) {
			   e.printStackTrace();
			  }
			  return holders; // returns the list of holders
	}

	
	String SQL2 = "SELECT * FROM account_holder WHERE account_holder_id = ?"; //query
	@Override
	public AccountHolder getAccount(int account_holder_id) { // getaccount_holder_by_id method which is overrided by accountholderDao interface
		AccountHolder accountholder = null;//initialize account_holder as null
		  try {
		   accountholder = jdbcTemplate.queryForObject(SQL2,new Object[] { account_holder_id }, new BeanPropertyRowMapper<AccountHolder>(AccountHolder.class));//to map the row of account_holders using bean property 
		  } catch (DataAccessException e) {
		   e.printStackTrace();
		  }
		  return accountholder; // return the accountholder
	}

	@Override
	public int deleteAccount(int account_holder_id) { // the delete method overrided by  accountholderDao interface
		// TODO Auto-generated method stub
		return 0;
	}

	
	String SQL4="UPDATE account_holder set account_holder_name = ? , address = ?,phone = ?,status_type_id = ?  where account_holder_id = ?";
	@Override
	public int updateAccount(AccountHolder accountholder) { // the update method ovrrided by by accountholderDao interface 
		int count = jdbcTemplate.update(SQL4, new Object[] {
			      accountholder.getAccount_holder_name(),accountholder.getAddress(),accountholder.getPhone(),accountholder.getStatus_type_id(),accountholder.getAccount_holder_id() });
			  return count;
	}

	String SQL3 = "INSERT INTO account_holder(account_holder_id,account_holder_name,address,phone,status_type_id)VALUES(?,?,?,?,?)";
	@Override
	public int createAccount(AccountHolder accountholder) { // the insert method overrided by by accountholderDao interface
		int count = jdbcTemplate.update(SQL3, new Object[] {
			     accountholder.getAccount_holder_id(),accountholder.getAccount_holder_name(),accountholder.getAddress(),accountholder.getPhone(),accountholder.getStatus_type_id() });
			  return count;
	}

	
	
	
	
}
