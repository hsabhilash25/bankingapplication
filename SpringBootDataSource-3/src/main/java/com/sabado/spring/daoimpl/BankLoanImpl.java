package com.sabado.spring.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.sabado.projects.jdbc.Jdbc;
import com.sabado.spring.dao.BankLoanDao;
import com.sabado.spring.entity.BankLoan;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountLoanEntityResponse;
import com.sabado.spring.entityResponse.LoanTypeEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;

@Repository
public class BankLoanImpl extends Jdbc implements BankLoanDao {
	



	@Override
	public List<AccountLoanEntityResponse> getAccountLoan() {
		
		String SQL	=  "SELECT a.account_id,ah.account_holder_id,ah.account_holder_name from account as a inner join account_holder as ah on a.account_holder_id = ah.account_holder_id" ;

		List<AccountLoanEntityResponse> accountloan = null;
		try {
			accountloan = jdbcTemplate.query(SQL, new RowMapper<AccountLoanEntityResponse>() {
				
				
				public AccountLoanEntityResponse mapRow(ResultSet rs,int i) throws SQLException{
					
					AccountLoanEntityResponse accountloans = new AccountLoanEntityResponse();
					accountloans.setAccount_holder_id(rs.getInt("account_holder_id"));
					accountloans.setAccount_id(rs.getInt("account_id"));
					
					accountloans.setAccount_holder_name(rs.getString("account_holder_name"));
					
					List<LoanTypeEntityResponse> loan = getLoanType(rs.getInt("account_id"));
					
					
					accountloans.setLoans(loan);
					return accountloans;
					
					
					
					
					
				   
				}
			});
			
			
			
			
		}catch(Exception e) {
			System.out.println(e);
			throw e;
			
		}
		
		return accountloan;
		       
	}


	@Override
	public List<LoanTypeEntityResponse> getLoanType(int account_id) {
		
		String	SQL	= "select l.loan_id,l.loan_amount,l.loan_amount+(loan_amount*duration*rate_of_interest/100)  as payment_amount, \r\n" + 
				" l.loan_amount+((loan_amount*duration*rate_of_interest/100)-l.loan_amount)  as interest_paying,\r\n" + 
				" lt.loan_type_id,lt.loan_type_name,dr.rate_of_interest,dr.duration from loan as l inner\r\n" + 
				"join loan_types as lt on l.loan_type_id = lt.loan_type_id inner join duration_rate as dr on lt.duration_rate_id =  \r\n" + 
				"				dr.duration_rate_id where l.account_id = ?";
	    List<LoanTypeEntityResponse> loan = jdbcTemplate.query(SQL,new Object[] {account_id}, new BeanPropertyRowMapper<LoanTypeEntityResponse>(LoanTypeEntityResponse.class));
		return loan;
		
	}
		
		
		
	}
    
    
    
    
    
    
   
   


