package com.sabado.spring.entityResponse;

public class TypesEntityResponse {
	private String account_type_name;

	public TypesEntityResponse(String account_type_name) {
		super();
		this.account_type_name = account_type_name;
	}

	public TypesEntityResponse() {
		super();
	}

	public String getAccount_type_name() {
		return account_type_name;
	}

	public void setAccount_type_name(String account_type_name) {
		this.account_type_name = account_type_name;
	}

}
