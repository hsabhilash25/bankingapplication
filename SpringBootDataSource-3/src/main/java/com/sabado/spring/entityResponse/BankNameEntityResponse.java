package com.sabado.spring.entityResponse;

import java.util.List;

public class BankNameEntityResponse {
 
	private int bank_id;
	private String bank_name;
	private List<BranchNameEntityResponse> branch;
	public BankNameEntityResponse() {
		super();
	}
	public BankNameEntityResponse(int bank_id, String bank_name, List<BranchNameEntityResponse> branch) {
		super();
		this.bank_id = bank_id;
		this.bank_name = bank_name;
		this.branch = branch;
	}
	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public List<BranchNameEntityResponse> getBranch() {
		return branch;
	}
	public void setBranch(List<BranchNameEntityResponse> branch) {
		this.branch = branch;
	}
	
	
	
	
}
