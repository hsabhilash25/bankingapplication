package com.sabado.spring.entityResponse;

public class CardAccountEntityResponse {
	private String card_type;

	public CardAccountEntityResponse() {
		super();
	}

	public CardAccountEntityResponse(String card_type) {
		super();
		this.card_type = card_type;
	}

	public String getCard_type() {
		return card_type;
	}

	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	

}
