package com.sabado.spring.entityResponse;

public class BranchNameEntityResponse {
	
private String branch_name;

public BranchNameEntityResponse() {
	super();
}

public BranchNameEntityResponse(String branch_name) {
	super();
	this.branch_name = branch_name;
}

public String getBranch_name() {
	return branch_name;
}

public void setBranch_name(String branch_name) {
	this.branch_name = branch_name;
}

	
	
	 
	

}
