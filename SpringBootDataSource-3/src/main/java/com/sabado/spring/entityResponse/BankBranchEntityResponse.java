package com.sabado.spring.entityResponse;

import java.util.List;

public class BankBranchEntityResponse {
	
	
	private int account_id;
	private String account_holder_name;
	private String account_type_name;
	private String Status_name;
	private List<CardAccountEntityResponse> cardaccount;
	public BankBranchEntityResponse() {
		super();
	}
	public BankBranchEntityResponse(int account_id, String account_holder_name, String account_type_name,
			String status_name, List<CardAccountEntityResponse> cardaccount) {
		super();
		this.account_id = account_id;
		this.account_holder_name = account_holder_name;
		this.account_type_name = account_type_name;
		Status_name = status_name;
		this.cardaccount = cardaccount;
	}
	public int getAccount_id() {
		return account_id;
	}
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public String getAccount_type_name() {
		return account_type_name;
	}
	public void setAccount_type_name(String account_type_name) {
		this.account_type_name = account_type_name;
	}
	public String getStatus_name() {
		return Status_name;
	}
	public void setStatus_name(String status_name) {
		Status_name = status_name;
	}
	public List<CardAccountEntityResponse> getCardaccount() {
		return cardaccount;
	}
	public void setCardaccount(List<CardAccountEntityResponse> cardaccount) {
		this.cardaccount = cardaccount;
	}
	
	
	

}
