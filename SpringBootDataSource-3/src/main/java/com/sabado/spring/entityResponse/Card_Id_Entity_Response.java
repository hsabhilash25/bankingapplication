package com.sabado.spring.entityResponse;

public class Card_Id_Entity_Response {
	
	private String card_type;

	public Card_Id_Entity_Response() {
		super();
	}

	public Card_Id_Entity_Response(String card_type) {
		super();
		this.card_type = card_type;
	}

	public String getCard_type() {
		return card_type;
	}

	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	

}
