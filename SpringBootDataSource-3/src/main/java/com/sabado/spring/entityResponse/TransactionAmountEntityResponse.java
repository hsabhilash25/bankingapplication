package com.sabado.spring.entityResponse;

import java.sql.Timestamp;

public class TransactionAmountEntityResponse {
	
	private String transaction_type;
	private int transaction_amount;
	private Timestamp transaction_date_time;
	public TransactionAmountEntityResponse() {
		super();
	}
	public TransactionAmountEntityResponse(String transaction_type, int transaction_amount,
			Timestamp transaction_date_time) {
		super();
		this.transaction_type = transaction_type;
		this.transaction_amount = transaction_amount;
		this.transaction_date_time = transaction_date_time;
	}
	public String getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}
	public int getTransaction_amount() {
		return transaction_amount;
	}
	public void setTransaction_amount(int transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	public Timestamp getTransaction_date_time() {
		return transaction_date_time;
	}
	public void setTransaction_date_time(Timestamp transaction_date_time) {
		this.transaction_date_time = transaction_date_time;
	}
	
	

}
