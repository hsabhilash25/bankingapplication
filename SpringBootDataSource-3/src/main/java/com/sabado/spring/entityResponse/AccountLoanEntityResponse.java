package com.sabado.spring.entityResponse;

import java.util.List;

public class AccountLoanEntityResponse {
	
	private int account_holder_id;
	private int account_id;
	private String account_holder_name;
	private List<LoanTypeEntityResponse> loans;
	public AccountLoanEntityResponse() {
		super();
	}
	public AccountLoanEntityResponse(int account_holder_id, int account_id, String account_holder_name,
			List<LoanTypeEntityResponse> loans) {
		super();
		this.account_holder_id = account_holder_id;
		this.account_id = account_id;
		this.account_holder_name = account_holder_name;
		this.loans = loans;
	}
	public int getAccount_holder_id() {
		return account_holder_id;
	}
	public void setAccount_holder_id(int account_holder_id) {
		this.account_holder_id = account_holder_id;
	}
	public int getAccount_id() {
		return account_id;
	}
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public List<LoanTypeEntityResponse> getLoans() {
		return loans;
	}
	public void setLoans(List<LoanTypeEntityResponse> loans) {
		this.loans = loans;
	}
	
	

}
