package com.sabado.spring.entityResponse;

import java.util.List;

public class AccountHolderEntityResponse {

	
	private int branch_id;
	private String branch_name;
	private String ifsc;
	private int account_holders_in_branch;
	private List<BankBranchEntityResponse> accounts;
	public AccountHolderEntityResponse() {
		super();
	}
	public AccountHolderEntityResponse(int branch_id, String branch_name, String ifsc, int account_holders_in_branch,
			List<BankBranchEntityResponse> accounts) {
		super();
		this.branch_id = branch_id;
		this.branch_name = branch_name;
		this.ifsc = ifsc;
		this.account_holders_in_branch = account_holders_in_branch;
		this.accounts = accounts;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public int getAccount_holders_in_branch() {
		return account_holders_in_branch;
	}
	public void setAccount_holders_in_branch(int account_holders_in_branch) {
		this.account_holders_in_branch = account_holders_in_branch;
	}
	public List<BankBranchEntityResponse> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<BankBranchEntityResponse> accounts) {
		this.accounts = accounts;
	}
	
	
	
	
	
}
