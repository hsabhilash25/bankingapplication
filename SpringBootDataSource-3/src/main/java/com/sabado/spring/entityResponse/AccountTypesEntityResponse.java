package com.sabado.spring.entityResponse;

import java.util.List;

public class AccountTypesEntityResponse {
	
	private int account_id;
	private int account_holder_id;
	private String account_holder_name;
	private List<TypesEntityResponse> Types;
	public AccountTypesEntityResponse() {
		super();
	}
	public AccountTypesEntityResponse(int account_id, int account_holder_id, String account_holder_name,
			List<TypesEntityResponse> types) {
		super();
		this.account_id = account_id;
		this.account_holder_id = account_holder_id;
		this.account_holder_name = account_holder_name;
		Types = types;
	}
	public int getAccount_id() {
		return account_id;
	}
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public int getAccount_holder_id() {
		return account_holder_id;
	}
	public void setAccount_holder_id(int account_holder_id) {
		this.account_holder_id = account_holder_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public List<TypesEntityResponse> getTypes() {
		return Types;
	}
	public void setTypes(List<TypesEntityResponse> types) {
		Types = types;
	}
	
	
	

}
