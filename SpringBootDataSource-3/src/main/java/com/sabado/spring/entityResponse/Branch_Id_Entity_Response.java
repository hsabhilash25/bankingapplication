package com.sabado.spring.entityResponse;

import java.util.List;

public class Branch_Id_Entity_Response {
	private int branch_id;
	private String branch_name;
	private String ifsc;
	private int account_holders_in_branch;
	private List<Account_Id_Entity_Response> account;
	public Branch_Id_Entity_Response() {
		super();
	}
	public Branch_Id_Entity_Response(int branch_id, String branch_name, String ifsc, int account_holders_in_branch,
			List<Account_Id_Entity_Response> account) {
		super();
		this.branch_id = branch_id;
		this.branch_name = branch_name;
		this.ifsc = ifsc;
		this.account_holders_in_branch = account_holders_in_branch;
		this.account = account;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public int getAccount_holders_in_branch() {
		return account_holders_in_branch;
	}
	public void setAccount_holders_in_branch(int account_holders_in_branch) {
		this.account_holders_in_branch = account_holders_in_branch;
	}
	public List<Account_Id_Entity_Response> getAccount() {
		return account;
	}
	public void setAccount(List<Account_Id_Entity_Response> account) {
		this.account = account;
	}
	
	

}
