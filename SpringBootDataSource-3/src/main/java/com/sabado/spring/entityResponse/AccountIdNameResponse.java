package com.sabado.spring.entityResponse;

import java.util.List;

public class AccountIdNameResponse {
	
	private int account_id;
	private String account_holder_name;
	private List<TransactionAmountEntityResponse> transaction;
	public AccountIdNameResponse() {
		super();
	}
	public AccountIdNameResponse(int account_id, String account_holder_name,
			List<TransactionAmountEntityResponse> transaction) {
		super();
		this.account_id = account_id;
		this.account_holder_name = account_holder_name;
		this.transaction = transaction;
	}
	public int getAccount_id() {
		return account_id;
	}
	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public List<TransactionAmountEntityResponse> getTransaction() {
		return transaction;
	}
	public void setTransaction(List<TransactionAmountEntityResponse> transaction) {
		this.transaction = transaction;
	}
	

}
