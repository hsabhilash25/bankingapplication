package com.sabado.spring.entityResponse;

import java.util.List;

public class BankEntityResponse {
	
	private int bank_id;
	private String bank_name;
	private List<AccountHolderEntityResponse> branch_details;
	public BankEntityResponse() {
		super();
	}
	public BankEntityResponse(int bank_id, String bank_name, List<AccountHolderEntityResponse> branch_details) {
		super();
		this.bank_id = bank_id;
		this.bank_name = bank_name;
		this.branch_details = branch_details;
	}
	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public List<AccountHolderEntityResponse> getBranch_details() {
		return branch_details;
	}
	public void setBranch_details(List<AccountHolderEntityResponse> branch_details) {
		this.branch_details = branch_details;
	}
	

}
