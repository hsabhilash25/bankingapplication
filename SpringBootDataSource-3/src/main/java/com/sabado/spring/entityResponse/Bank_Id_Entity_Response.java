package com.sabado.spring.entityResponse;

import java.util.List;

public class Bank_Id_Entity_Response {
	private int bank_id;
	private String bank_name;
	private List<Branch_Id_Entity_Response> branch;
	public Bank_Id_Entity_Response() {
		super();
	}
	public Bank_Id_Entity_Response(int bank_id, String bank_name, List<Branch_Id_Entity_Response> branch) {
		super();
		this.bank_id = bank_id;
		this.bank_name = bank_name;
		this.branch = branch;
	}
	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public List<Branch_Id_Entity_Response> getBranch() {
		return branch;
	}
	public void setBranch(List<Branch_Id_Entity_Response> branch) {
		this.branch = branch;
	}
	
	
	
	

}
