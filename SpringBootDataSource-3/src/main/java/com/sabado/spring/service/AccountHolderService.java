package com.sabado.spring.service;

import java.util.List;

import com.sabado.spring.entity.AccountHolder;

public interface AccountHolderService {
	
	 public List<AccountHolder> getAccountholder();
	 public AccountHolder getAccount(int account_holder_id);
	 public int deleteAccount(int account_holder_id); 
	 public int updateAccount(AccountHolder accountholder);
	 public int createAccount(AccountHolder accountholder);

}
