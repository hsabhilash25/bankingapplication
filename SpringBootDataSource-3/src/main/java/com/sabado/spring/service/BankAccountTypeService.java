package com.sabado.spring.service;

import java.util.List;

import com.sabado.spring.entity.BankAccountType;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountTypesEntityResponse;

public interface BankAccountTypeService {
	
	public List<AccountTypesEntityResponse> getAccountType();

}
