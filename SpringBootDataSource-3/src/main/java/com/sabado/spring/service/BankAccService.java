package com.sabado.spring.service;

import java.util.List;

import com.sabado.spring.entity.BankAccount;
import com.sabado.spring.entityResponse.AccountHolderEntityResponse;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.BankEntityResponse;

public interface BankAccService {
	public List<BankEntityResponse> getBankEntity();

}
