package com.sabado.spring.service;

import java.util.List;

import com.sabado.spring.entityResponse.BankNameEntityResponse;

public interface BankBranchService {
	public List<BankNameEntityResponse> getBankNamesById();
	 

}
