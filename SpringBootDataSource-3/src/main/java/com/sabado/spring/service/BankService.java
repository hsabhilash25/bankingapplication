package com.sabado.spring.service;

import java.util.List;

import com.sabado.spring.entity.Bank;

public interface BankService {
	
	public List<Bank> getBankNames();
	 public Bank getBankId(int bank_id);
	 public int deleteBankId(int bank_id); 
	 public int updateBankId(Bank bank);
	 public int createBankNames(Bank bank);

}
