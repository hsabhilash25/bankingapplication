package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entity.Bank;
import com.sabado.spring.exception.NoMatchFoundException;
import com.sabado.spring.service.BankService;

@RestController
public class BankController {

	@Autowired
	private BankService bankservice;

	@RequestMapping(value = "/bank", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Bank>> banks() {

		HttpHeaders headers = new HttpHeaders();
		List<Bank> banks = bankservice.getBankNames();

		if (banks == null) {
			return new ResponseEntity<List<Bank>>(HttpStatus.NOT_FOUND);
		}
		headers.add("Number Of Records Found", String.valueOf(banks.size()));
		return new ResponseEntity<List<Bank>>(banks, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/bank/{bank_id}", method = RequestMethod.GET)
	public ResponseEntity<Bank> getBank(@PathVariable("bank_id") int bank_id) {
		Bank bank = bankservice.getBankId(bank_id);
		if (bank == null) {
			return new ResponseEntity<Bank>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Bank>(bank, HttpStatus.OK);
	}

	@RequestMapping(value = "/bank/delete/{bank_id}", method = RequestMethod.DELETE)
	public ResponseEntity<Bank> deleteBank(@PathVariable("bank_id") int bank_id) {
		HttpHeaders headers = new HttpHeaders();
		Bank bank = bankservice.getBankId(bank_id);
		if (bank == null) {
			return new ResponseEntity<Bank>(HttpStatus.NOT_FOUND);
		}
		bankservice.deleteBankId(bank_id);
		headers.add("Bank Deleted - ", String.valueOf(bank_id));
		return new ResponseEntity<Bank>(bank, headers, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/bank", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Bank> createBank(@RequestBody Bank bank) {
		HttpHeaders headers = new HttpHeaders();
	 if (bank == null) {
	   return new ResponseEntity<Bank>(HttpStatus.BAD_REQUEST);
	  }
	  bankservice.createBankNames(bank);
	  headers.add("Bank Created  - ", String.valueOf(bank.getBank_id()));
	  return new ResponseEntity<Bank>(bank, headers, HttpStatus.CREATED);
	 }
	
	
	
	

	@RequestMapping(value = "/bank/{bank_id}", method = RequestMethod.PUT)
	public ResponseEntity<Bank> updateBank(@PathVariable("bank_id") int bank_id, @RequestBody Bank bank) {
		HttpHeaders headers = new HttpHeaders();
		Bank isExist = bankservice.getBankId(bank_id);
		if (isExist == null) {
			return new ResponseEntity<Bank>(HttpStatus.NOT_FOUND);
		} else if (bank == null) {
			return new ResponseEntity<Bank>(HttpStatus.BAD_REQUEST);
		}
		bankservice.updateBankId(bank);
		headers.add("bank Updated  - ", String.valueOf(bank_id));
		return new ResponseEntity<Bank>(bank, headers, HttpStatus.OK);
	}

}
