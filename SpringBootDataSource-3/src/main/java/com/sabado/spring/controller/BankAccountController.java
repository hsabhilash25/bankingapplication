package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entity.BankAccount;
import com.sabado.spring.entityResponse.AccountHolderEntityResponse;
import com.sabado.spring.entityResponse.BankEntityResponse;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.service.BankAccService;

@RestController
public class BankAccountController {
	
	@Autowired
    public BankAccService bankaccservice;

    @RequestMapping("/bankaccount")
    public List<BankEntityResponse> getBankEntity(){
    	
    	List<BankEntityResponse> accounts = bankaccservice.getBankEntity();
		return accounts;
		
    }
	
   
	

}
