package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entity.BankAccountType;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountTypesEntityResponse;
import com.sabado.spring.service.BankAccountTypeService;

@RestController
public class BankAccountTypeController {
	
	@Autowired
    public BankAccountTypeService bankaccounttypeservice;

    @RequestMapping("/bankaccounttype")
    public List<AccountTypesEntityResponse> getAccountTypes() {
        List<AccountTypesEntityResponse> accounttypes = bankaccounttypeservice.getAccountType();
		return accounttypes;
		
		
        
        
    }
    

}