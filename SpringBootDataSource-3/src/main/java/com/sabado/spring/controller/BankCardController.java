package com.sabado.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sabado.spring.entityResponse.AccountTypesEntityResponse;
import com.sabado.spring.entityResponse.Bank_Id_Entity_Response;
import com.sabado.spring.service.BankAccountTypeService;
import com.sabado.spring.service.BankCardService;

@RestController
public class BankCardController {
	

	@Autowired
    public BankCardService bankcardservice;

    @RequestMapping("/bankcard")
    public List<Bank_Id_Entity_Response> getBankCards() {
        List<Bank_Id_Entity_Response> accounttypes = bankcardservice.getBankById();
		return accounttypes;
	
    }
	
	
	

}
