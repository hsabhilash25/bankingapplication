package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.entityResponse.BranchNameEntityResponse;

public interface BankBranchesDao {
	 public List<BankNameEntityResponse> getBankBranch();
	 
	 public List<BranchNameEntityResponse> getBranchNames(int bank_id);
	 
	 
	 

}
