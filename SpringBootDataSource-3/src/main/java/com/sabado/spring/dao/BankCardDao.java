package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entityResponse.Account_Id_Entity_Response;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.entityResponse.Bank_Id_Entity_Response;
import com.sabado.spring.entityResponse.Branch_Id_Entity_Response;
import com.sabado.spring.entityResponse.Card_Id_Entity_Response;

public interface BankCardDao {
	 public List<Bank_Id_Entity_Response> getBankId();
	 public List<Branch_Id_Entity_Response> getBranchId(int bank_id);
	 public List<Account_Id_Entity_Response> getAccountId(int branch_id);
	 public List<Card_Id_Entity_Response> getCardId(int account_id);

}
