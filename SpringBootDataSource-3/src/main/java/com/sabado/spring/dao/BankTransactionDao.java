package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entity.BankTransaction;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.BranchNameEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;

public interface BankTransactionDao {
	
	public List<AccountIdNameResponse> getAccountAmount();
	
	
	 public List<TransactionAmountEntityResponse> getTransactionAmount(int account_id);
	
	

}
