package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entity.BankAccount;
import com.sabado.spring.entityResponse.AccountHolderEntityResponse;
import com.sabado.spring.entityResponse.BankBranchEntityResponse;
import com.sabado.spring.entityResponse.BankEntityResponse;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.entityResponse.BranchNameEntityResponse;
import com.sabado.spring.entityResponse.CardAccountEntityResponse;

public interface BankAccDao {
	
	public List<BankEntityResponse> getBankEntity();
	
	public List<AccountHolderEntityResponse> getBankAccount(int bank_id);
	 
	 public List<BankBranchEntityResponse> getBankId(int branch_id);
	 
	 public List<CardAccountEntityResponse> getCardName(int account_id);
	 
	 
	 

}
