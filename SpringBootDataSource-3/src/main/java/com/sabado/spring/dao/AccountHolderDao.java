package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entity.AccountHolder;


public interface AccountHolderDao {

	 public List<AccountHolder> getAccounts();
	 public AccountHolder getAccount(int account_holder_id);
	 public int deleteAccount(int account_holder_id); 
	 public int updateAccount(AccountHolder accountholder);
	 public int createAccount(AccountHolder accountholder);
	
}
