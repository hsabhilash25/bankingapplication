package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entity.BankAccountType;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountTypesEntityResponse;
import com.sabado.spring.entityResponse.TransactionAmountEntityResponse;
import com.sabado.spring.entityResponse.TypesEntityResponse;

public interface BankAccountTypeDao {
	
	public List<AccountTypesEntityResponse> getAccountTypes();
	
	
	 public List<TypesEntityResponse> getTypesEntity(int account_holder_id);

}
