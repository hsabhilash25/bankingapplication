package com.sabado.spring.dao;

import java.util.List;

import com.sabado.spring.entity.Bank;

public interface BankDao {
	     public List<Bank> getBanks();
		 public Bank getBank(int bank_id);
		 public int deleteBank(int bank_id); 
		 public int updateBank(Bank bank);
		 public int createBank(Bank bank);
		 
		
		 
		 

	}


