package com.sabado.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataSource3Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataSource3Application.class, args);
	}

}
