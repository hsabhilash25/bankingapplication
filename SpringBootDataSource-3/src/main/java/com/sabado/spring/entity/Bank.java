package com.sabado.spring.entity;




public class Bank {

	
	private int bank_id;
	 private String bank_name;
	 private int location_id;
	 private String address;
	 private String ifsc;
	 private int bank_type_id;
	 
	 
	 



	public Bank() {
		super();
	}






	public int getBank_id() {
		return bank_id;
	}






	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}






	public String getBank_name() {
		return bank_name;
	}






	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}






	public int getLocation_id() {
		return location_id;
	}






	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}






	public String getAddress() {
		return address;
	}






	public void setAddress(String address) {
		this.address = address;
	}






	public String getIfsc() {
		return ifsc;
	}






	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}






	public int getBank_type_id() {
		return bank_type_id;
	}






	public void setBank_type_id(int bank_type_id) {
		this.bank_type_id = bank_type_id;
	}






	@Override
	 public String toString() {
	  StringBuilder str = new StringBuilder();
	  str.append("bank_id:- " +  getBank_id());
	  str.append(" Name :- " + getBank_name());
	  str.append(" location_id :- " + getLocation_id());
	  str.append(" address:- " + getAddress());
	  str.append(" ifsc:- " + getIfsc());
	  str.append(" bank_type_id:- " + getBank_type_id());
	  
	  
			  
	  return str.toString();
	 }
	 
	 
}
