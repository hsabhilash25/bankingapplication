package com.sabado.spring.entity;

public class BankTransaction {
	
	private int account_id;
	private int account_holder_id;
    private String account_holder_name;
    private String transaction_type;
    private int transaction_amount;
    
	public BankTransaction() {
		super();
	}

	public BankTransaction(int account_id, int account_holder_id, String account_holder_name, String transaction_type,
			int transaction_amount) {
		super();
		this.account_id = account_id;
		this.account_holder_id = account_holder_id;
		this.account_holder_name = account_holder_name;
		this.transaction_type = transaction_type;
		this.transaction_amount = transaction_amount;
	}

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public int getAccount_holder_id() {
		return account_holder_id;
	}

	public void setAccount_holder_id(int account_holder_id) {
		this.account_holder_id = account_holder_id;
	}

	public String getAccount_holder_name() {
		return account_holder_name;
	}

	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}

	public String getTransaction_type() {
		return transaction_type;
	}

	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}

	public int getTransaction_amount() {
		return transaction_amount;
	}

	public void setTransaction_amount(int transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
    
    
	
	

}
