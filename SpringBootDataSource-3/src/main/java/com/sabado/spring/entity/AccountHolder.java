package com.sabado.spring.entity;

public class AccountHolder {

	private int account_holder_id;
	private String account_holder_name;
	private String address;
	private Long phone;
	private int status_type_id;
	
	
	//create the constructor
	public AccountHolder() {
		super();
	}
	
	//create parameterized constructor
	public AccountHolder(int account_holder_id, String account_holder_name, String address, Long phone,
			int status_type_id) {
		super();
		
		this.account_holder_id = account_holder_id;
		this.account_holder_name = account_holder_name;
		this.address = address;
		this.phone = phone;
		this.status_type_id = status_type_id;
	}
	
	//create getter and setters
	public int getAccount_holder_id() {
		return account_holder_id;
	}
	public void setAccount_holder_id(int account_holder_id) {
		this.account_holder_id = account_holder_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getPhone() {
		return phone;
	}
	public void setPhone(Long phone) {
		this.phone = phone;
	}
	public int getStatus_type_id() {
		return status_type_id;
	}
	public void setStatus_type_id(int status_type_id) {
		this.status_type_id = status_type_id;
	}
	
	
	
	
	
}
