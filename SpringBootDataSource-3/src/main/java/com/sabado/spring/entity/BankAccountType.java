package com.sabado.spring.entity;

public class BankAccountType {
	
	private int account_id;
	private int account_holder_id;
    private String account_holder_name;
   private String account_type_name;
   
public BankAccountType() {
	super();
}

public BankAccountType(int account_id, int account_holder_id, String account_holder_name, String account_type_name) {
	super();
	this.account_id = account_id;
	this.account_holder_id = account_holder_id;
	this.account_holder_name = account_holder_name;
	this.account_type_name = account_type_name;
}

public int getAccount_id() {
	return account_id;
}

public void setAccount_id(int account_id) {
	this.account_id = account_id;
}

public int getAccount_holder_id() {
	return account_holder_id;
}

public void setAccount_holder_id(int account_holder_id) {
	this.account_holder_id = account_holder_id;
}

public String getAccount_holder_name() {
	return account_holder_name;
}

public void setAccount_holder_name(String account_holder_name) {
	this.account_holder_name = account_holder_name;
}

public String getAccount_type_name() {
	return account_type_name;
}

public void setAccount_type_name(String account_type_name) {
	this.account_type_name = account_type_name;
}
	

   
   

}
