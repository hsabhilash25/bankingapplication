package com.sabado.spring.entity;

public class BankAccount {
	
	private int account_holder_id;
    private String account_holder_name;
    private String branch_name ;
	private int account_id;
	private String bank_name;
	
	public BankAccount() {
		super();
	}

	public BankAccount(int account_holder_id, String account_holder_name, String branch_name, int account_id,
			String bank_name) {
		super();
		this.account_holder_id = account_holder_id;
		this.account_holder_name = account_holder_name;
		this.branch_name = branch_name;
		this.account_id = account_id;
		this.bank_name = bank_name;
	}

	public int getAccount_holder_id() {
		return account_holder_id;
	}

	public void setAccount_holder_id(int account_holder_id) {
		this.account_holder_id = account_holder_id;
	}

	public String getAccount_holder_name() {
		return account_holder_name;
	}

	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}

	public String getBranch_name() {
		return branch_name;
	}

	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	
	
	
	

}
