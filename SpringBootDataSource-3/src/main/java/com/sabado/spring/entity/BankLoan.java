package com.sabado.spring.entity;

public class BankLoan {
	
	private int account_holder_id;
    private String account_holder_name;
    private int account_id;
    private int loan_id;
	private String loan_type_name;
	
	
	public BankLoan() {
		super();
	}


	public BankLoan(int account_holder_id, String account_holder_name, int account_id, int loan_id,
			String loan_type_name) {
		super();
		this.account_holder_id = account_holder_id;
		this.account_holder_name = account_holder_name;
		this.account_id = account_id;
		this.loan_id = loan_id;
		this.loan_type_name = loan_type_name;
	}


	public int getAccount_holder_id() {
		return account_holder_id;
	}


	public void setAccount_holder_id(int account_holder_id) {
		this.account_holder_id = account_holder_id;
	}


	public String getAccount_holder_name() {
		return account_holder_name;
	}


	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}


	public int getAccount_id() {
		return account_id;
	}


	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}


	public int getLoan_id() {
		return loan_id;
	}


	public void setLoan_id(int loan_id) {
		this.loan_id = loan_id;
	}


	public String getLoan_type_name() {
		return loan_type_name;
	}


	public void setLoan_type_name(String loan_type_name) {
		this.loan_type_name = loan_type_name;
	}
	
	
	
    
}
