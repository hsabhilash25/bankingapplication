package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.AccountHolderDao;
import com.sabado.spring.entity.AccountHolder;
import com.sabado.spring.service.AccountHolderService;

@Service("accountholderservice")
public class AccountHolderServiceImpl implements  AccountHolderService {
	
	@Autowired //to inject the account_holder_dao object dependency implicitly
	 private AccountHolderDao accountholderDao;


	@Override
	public List<AccountHolder> getAccountholder() {
		List<AccountHolder> holders = accountholderDao.getAccounts();//to send the request to account_holderDao layer
		  return holders;
	}

	@Override
	public AccountHolder getAccount(int account_holder_id) {
		AccountHolder accountholder = accountholderDao.getAccount(account_holder_id);//to send the request to account_holderDao layer
		  return accountholder;
	}

	@Override
	public int deleteAccount(int account_holder_id) {
		return accountholderDao.deleteAccount(account_holder_id);//to send the request to account_holderDao layer
	}

	@Override
	public int updateAccount(AccountHolder accountholder) {
		return accountholderDao.updateAccount(accountholder);//to send the request to account_holderDao layer
	}

	@Override
	public int createAccount(AccountHolder accountholder) {
		return accountholderDao.createAccount(accountholder);//to send the request to account_holderDao layer
	}


	
}
