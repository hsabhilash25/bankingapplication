package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankDao;
import com.sabado.spring.entity.Bank;
import com.sabado.spring.service.BankService;

@Service("bankservice")
public class BankServiceImpl implements BankService {
	
	@Autowired
	private BankDao bankDao;

	@Override
	public List<Bank> getBankNames() {
		List<Bank> banks = bankDao.getBanks();
		  return banks;
	}

	@Override
	public Bank getBankId(int bank_id) {
		Bank bank = bankDao.getBank(bank_id);
		  return bank;
	}

	@Override
	public int deleteBankId(int bank_id) {
		return bankDao.deleteBank(bank_id);
	}

	@Override
	public int updateBankId(Bank bank) {
	return bankDao.updateBank(bank);
	}

	@Override
	public int createBankNames(Bank bank) {
		return bankDao.createBank(bank);
	}

	

}
