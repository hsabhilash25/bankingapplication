package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankLoanDao;
import com.sabado.spring.entity.BankLoan;
import com.sabado.spring.entityResponse.AccountIdNameResponse;
import com.sabado.spring.entityResponse.AccountLoanEntityResponse;
import com.sabado.spring.service.BankLoanService;

@Service("getloans")
public class BankLoanServiceImpl implements BankLoanService {
	
	@Autowired
	private BankLoanDao bankloanDao;



	@Override
	public List<AccountLoanEntityResponse> getAccountLoanEntity() {
		List<AccountLoanEntityResponse> accountloan = bankloanDao.getAccountLoan();
		return accountloan;
		
	}



	



}
