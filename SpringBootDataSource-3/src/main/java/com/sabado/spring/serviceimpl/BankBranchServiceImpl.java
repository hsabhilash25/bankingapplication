package com.sabado.spring.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sabado.spring.dao.BankBranchesDao;
import com.sabado.spring.entityResponse.BankNameEntityResponse;
import com.sabado.spring.service.BankBranchService;
@Service("bankbranchesservices")
public class BankBranchServiceImpl implements BankBranchService{
	
	@Autowired
	private BankBranchesDao bankbranchesDao;


	@Override
	public List<BankNameEntityResponse> getBankNamesById() {
		List<BankNameEntityResponse> banknames = bankbranchesDao.getBankBranch();
		return banknames;
	}

	


}
